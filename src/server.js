const express = require("express");
const port = 3000;
const app = express();

app.get("/", (req, res) => res.send("welcome"));
// add two numbers
app.get("/add", (req,res) => {
    try{
        console.log({
            a:parseInt(req.query.a),
            b:parseInt(req.query.b)

        });
        const sum = parseInt(req.query.a) + parseInt(req.query.b);
        res.send(sum.toString()); //Server internal error
    }catch(e){
        res.sendStatus(500); //Server internal error
    }
});

if (process.env.NODE_ENV === "test"){
    module.exports = app;
}
if(!module.parent){

app.listen(port, ()=> console.log(`Server running at localhost:${port}`));
}